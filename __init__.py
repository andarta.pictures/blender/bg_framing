# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "BG_Framing",
    "author" : "Tom VIGUIER",
    "description" : "",
    "blender" : (3, 6, 0),
    "version" : (0, 0, 1),
    "location" : "",
    "warning" : "",
    "category" : "Generic"
}
import bpy
import bpy
import numpy as np
from mathutils import Vector, Matrix, Euler

def assign_matrix_basis(ob, matrix, frame_nb):
    loc, rot, sca = matrix.decompose()
    ##print('assign to', ob.name, loc, rot.to_euler(), sca)

    ob.location = loc
    ob.rotation_euler = rot.to_euler()
    ob.scale = sca
    return



def snap_parented_object(ob, target_matrix_world):
    ##print('---------------------------------SNAP', ob.name, ob.users_scene) 
    ##print('target')
    ##print(target_matrix_world)  
    #determine parent
    bpy.context.view_layer.update()
    parent = None
    for c in ob.constraints :
        if c.type == 'CHILD_OF' and c.influence == 1 and c.target is not None:
            if c.target.type == 'ARMATURE' and c.subtarget != '' :
                parent = c.target.pose.bones[c.subtarget]
                inv_parent = Matrix(c.target.matrix_world @ parent.matrix)
            else :
                parent = c.target
                inv_parent = Matrix(parent.matrix_world)
            break
    if parent is None :
        inv_parent = Matrix()  
    
    #compute result
    else :
        ##print('PARENT = ', parent.name)
        
        inv_parent.invert()  
    result = Matrix(inv_parent @ target_matrix_world)
    #assign
    
    assign_matrix_basis(ob, result, bpy.context.scene.frame_current)
    #bpy.context.view_layer.update()
    ##print('result')
    ##print(ob.matrix_world)
    return

class BG_FRAMING_AddonPref(bpy.types.AddonPreferences):
    bl_idname = __name__
    max_size : bpy.props.IntProperty(default = 10000, subtype = 'PIXEL')
    

    def draw(self, context):
        layout = self.layout
        row = layout.row()

        


class BG_OT_CREATE_VL(bpy.types.Operator):
    '''create viewlayers for each BG frame'''
    bl_idname = "bg.create_vl"
    bl_label = "create viewlayers for each BG frame"
    bl_options = {'UNDO', 'REGISTER'}
    def execute(self,context):
        src_vl = context.scene.view_layers['CADRES']
        src_col = bpy.data.collections['CADRES']
        
        for col_shot in src_col.children : 
            #print(col_shot.name) #EPxxx_SHxxx    

            for col_cadre in col_shot.children :
                #print(col_cadre.name) #CADRE_A
                vl_name = col_shot.name + '_' + col_cadre.name[12:]


                #create new view_layer
                if vl_name in context.scene.view_layers.keys() :
                    context.scene.view_layers.remove(context.scene.view_layers[vl_name])
                new_vl = context.scene.view_layers.new(vl_name)
                for attr in dir(src_vl):
                    if attr not in ['name', 'use']:
                        try :
                            setattr(new_vl, attr, getattr(src_vl, attr))
                        except : 
                            pass
                new_vl.use = True
                for col2 in new_vl.layer_collection.children :
                    if col2.name != 'CADRES' :
                        col2.exclude = True
                    else :
                        col2.exclude = False

                        for col3 in col2.children :
                            if col3.name != col_shot.name :
                                col3.exclude = True
                            else : 
                                col3.exclude = False 

                                for col4 in col3.children :
                                    if col4.name != col_cadre.name :
                                        col4.exclude = True
                                    else :

                                        col4.exclude = False
        src_vl.use = False


        return {'FINISHED'}

class BG_OT_FRAMING(bpy.types.Operator):
    '''frame bg for photoshop reference'''
    bl_idname = "bg.framing"
    bl_label = "frame bg for photoshop reference"
    bl_options = {'UNDO', 'REGISTER'}
    
    
    def execute(self,context):

        #print('------START-----')
        col_cadres = bpy.data.collections['CADRES']
        cam_obj = context.scene.camera
        camera = cam_obj.data
        

        cadres = [c for c in col_cadres.all_objects if 'CADRE' in c.name]
        widths = []
        heights = []
        cadres_mats = []
        for i, cadre in enumerate(cadres) :
            #print(i, cadre)
            #print(cadre.matrix_basis)
            cadres_mats.append((cadre,Matrix(cadre.matrix_world)))
            points = np.array([cadre.matrix_basis @ v.co for v in cadre.data.vertices])
            
            ##print(points)
            if i == 0 :
                all_points = points
            else :
                all_points = np.concatenate((all_points,points),axis = 0)
            loc_max_x = np.max(points[:,0])
            loc_min_x = np.min(points[:,0])
            
            loc_max_y = np.max(points[:,1])
            loc_min_y = np.min(points[:,1])
            width = loc_max_x - loc_min_x
            widths.append(width)
            height = loc_max_y - loc_min_y
            heights.append(height)
            #print('width, height , ratio',width,height,width/height)
            
                #2136 x 1200
        #print('-------------------------------')
        min_width = widths[np.argmin(widths)]
        min_height = heights[np.argmin(heights)]
        #print('smallest width : ', np.argmin(widths))
        #print('smallest height : ', np.argmin(heights))

        conversion_ratio_x = 2136/min_width
        conversion_ratio_y = 1200/min_height

        max_x = np.max(all_points[:,0])
        min_x = np.min(all_points[:,0])
                
        max_y = np.max(all_points[:,1])
        min_y = np.min(all_points[:,1])
        ##print('IIIII', min_x, max_x, min_y,max_y)
        total_width = max_x - min_x 
        total_height = max_y - min_y
        #print('global width height and ratio :', total_width, total_height,total_width/total_height)


        res_x = int(total_width * conversion_ratio_x)
        res_y = int(total_height * conversion_ratio_y)
        preferences = context.preferences
        addon_prefs = preferences.addons[__name__].preferences
        if res_x > addon_prefs.max_size or res_y > addon_prefs.max_size :
            self.report({'ERROR'}, message = 'Maximum size exceeded')
            return{'CANCELLED'}

        default_w = 1.059442162513733
        #print('d', default_w)
        #print('f', total_width)
        focal_ratio = default_w / total_width
        #print(focal_ratio)


        #print('ratios : ', init_ratio, final_ratio)

        bpy.context.scene.render.resolution_x = res_x
        bpy.context.scene.render.resolution_y = res_y

        #centre du rectangle total : orienter la cam vers lui
        center = Vector((total_width/2 + min_x, total_height/2 + min_y, np.mean(all_points[:,2])))
        #empty.location = center 
        #cam_obj.rotation_euler[0] += angle
        center_world = cam_obj.matrix_world @ center

        center_from_cam = center_world - cam_obj.location

        euler_direction = center_from_cam.to_track_quat('-Z', 'Y').to_euler()
        #print(cam_obj.rotation_euler)
        #print(euler_direction)
        cam_obj.rotation_euler = euler_direction


        camera.lens *= focal_ratio

        for cadre, mat in cadres_mats :
            snap_parented_object(cadre,mat)
            cadre.rotation_euler = Euler()

            cadre.scale *= focal_ratio
            cadre.location[0] *= focal_ratio
            cadre.location[1] *= focal_ratio

        return {'FINISHED'}
    
class BGFRAMING_PT_PANEL(bpy.types.Panel): 
    bl_label = "Background framing"
    bl_idname = "BGFRAMING_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "BG_Framing"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        
        row.operator("bg.framing", text = 'Frame BG')
        row.prop(context.preferences.addons[__name__].preferences, 'max_size', text = 'Max size : ')

        row = layout.row()
        row.operator("bg.create_vl", text = 'Split view layers')  
        row = layout.row()
        row.prop(context.scene.render, 'filepath')
        row.prop(context.scene.render.image_settings, 'file_format')    
        row = layout.row()
        ops = row.operator('render.render')
        ops.write_still = True

 
classes = [ BG_OT_FRAMING,
           BGFRAMING_PT_PANEL,
           BG_OT_CREATE_VL,
           BG_FRAMING_AddonPref
            ]
def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
